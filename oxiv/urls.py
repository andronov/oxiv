"""oxiv URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic import TemplateView
from rest_framework import routers
from yandex_money.views import CheckOrderFormView
from yandex_money.views import NoticeFormView
from accounts.views import new_registration_user
from core.views import UserBalanceView, test_aviso, test_check_order

admin.autodiscover()



urlpatterns = [
    #url("", include("django_socketio.urls")),
    #url('^chat/', include("chat.urls")),

    url('', include('core.urls', namespace="core")),
    url('^adm/', include('padmin.urls', namespace="padmin")),
    url('^api/', include('api.urls', namespace="api_websocket")),
    url('^manager/', include('core.urls', namespace="manager")),

    url(r'^logout/$', 'django.contrib.auth.views.logout', name='logout'),

    url(r'^registration/', new_registration_user, name='new_registration_user'),

    url(r'^accounts/', include('registration.backends.default.urls')),

    url(r'^fail-payment/$', UserBalanceView.as_view(), name='payment_fail'),
    url(r'^success-payment/$', UserBalanceView.as_view(), name='payment_success'),

    #url(r'^yandex-money/aviso/$', test_aviso, name='test_aviso'),
    #url(r'^yandex-money/check/$', test_check_order, name='test_check_order'),
    url(r'^yandex-money/check/$', CheckOrderFormView.as_view(), name='yandex_money_check'),
    url(r'^yandex-money/aviso/$', NoticeFormView.as_view(), name='yandex_money_notice'),

    #url(r'^yandex-money/', include('yandex_money.urls')),


    url(r'^admin/', include(admin.site.urls)),



] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)\
              + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
