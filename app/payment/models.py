# -*- coding: utf-8 -*-
from django.db import models
from django.db.models.signals import post_save
from sorl.thumbnail import ImageField
from yandex_money.models import Payment
from accounts.models import UserProfile
from core.models import UserBids, Promocode

"""
#
# История платежей
#
"""
class Order(models.Model):
    user = models.ForeignKey(UserProfile, related_name='order_user', verbose_name=u'юзер')
    promocode = models.ForeignKey(Promocode, related_name='order_promocode', blank=True, null=True)

    amount = models.PositiveIntegerField(verbose_name=u'Сумма в оплаты')
    check = models.BooleanField(default=False)

    id_payment = models.IntegerField(blank=True, null=True)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-created']

"""
#
# История списаний
#
"""
class HistoryDebit(models.Model):
    user = models.ForeignKey(UserProfile, related_name='historydebit_user', verbose_name=u'юзер')
    leads = models.ForeignKey(UserBids, related_name='historydebit')
    amount = models.PositiveIntegerField(verbose_name=u'Сумма списания')
    check = models.BooleanField(default=False)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-created']

"""
#
# After success payment
#
"""
def after_succes_payment_order(sender, **kwargs):
    pay = kwargs["instance"]
    if pay.status == 'success':
        Order.objects.create(user=pay.user, amount=pay.order_amount, check=True, id_payment=pay.id)

post_save.connect(after_succes_payment_order, sender=Payment)