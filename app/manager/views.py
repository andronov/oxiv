# -*- coding: utf-8 -*-
from django.views.generic import TemplateView





"""
#
# Manager user list (Список пользователя)
#
"""
class ManagerUserListView(TemplateView):
    template_name = 'manager/users.html'

    def dispatch(self, *args, **kwargs):
        #if not self.request.user.is_authenticated():
                #return redirect('auth_login')
        return super(ManagerUserListView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ManagerUserListView, self).get_context_data(**kwargs)
        return context

    """
#
# A statistics' the manager (Полная статистика администратора )
#
"""
class ManagerStatisticsView(TemplateView):
    template_name = 'manager/statistics.html'

    def dispatch(self, *args, **kwargs):
        #if not self.request.user.is_authenticated():
                #return redirect('auth_login')
        return super(ManagerStatisticsView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ManagerStatisticsView, self).get_context_data(**kwargs)
        return context

"""
#
# Settings manager (Настройки пользователя)
#
"""
class ManagerSettingsView(TemplateView):
    template_name = 'manager/settings.html'

    def dispatch(self, *args, **kwargs):
        #if not self.request.user.is_authenticated():
                #return redirect('auth_login')
        return super(ManagerSettingsView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ManagerSettingsView, self).get_context_data(**kwargs)
        return context

"""
#
# The balance manager (Баланс пользователя)
#
"""
class ManagerBalanceView(TemplateView):
    template_name = 'manager/balance.html'

    def dispatch(self, *args, **kwargs):
        #if not self.request.user.is_authenticated():
                #return redirect('auth_login')
        return super(ManagerBalanceView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ManagerBalanceView, self).get_context_data(**kwargs)
        return context