from django.contrib import admin
from core.models import *

"""
class UserBidsAdmin(admin.ModelAdmin):
    fields = ('updated',)
    list_display = ('id', 'status_callback', 'created', )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('updated', 'created',),
        }),
    )
"""

admin.site.register(Topics)
admin.site.register(Bids)
admin.site.register(UserSettings)
admin.site.register(UserBids)
admin.site.register(Campaigns)
admin.site.register(StatusBids)
admin.site.register(ProductsAndService)
admin.site.register(Promocode)