# -*- coding: utf-8 -*-
from django.contrib.auth import user_logged_in
from django.contrib.sessions.models import Session
from django.db import models
from django.db.models.signals import post_save
from sorl.thumbnail import ImageField
from yandex_money.models import Payment
from accounts.models import UserProfile


class BaseModel(models.Model):
    is_active = models.BooleanField(default=True)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True

"""
#
# Topics
#
"""
class Topics(BaseModel):
    name = models.CharField(max_length=250, verbose_name='Name', null=False)

    class Meta:
        verbose_name = 'topic'
        verbose_name_plural = 'topics'

    # return расходов по кампаниям
    def expense(self):
        expense = 0
        for se in self.campaign_topic.all():
            expense += se.expense
        return self.expense

"""
#
# Campaigns
#
"""
class Campaigns(BaseModel):
    CHOICE_TYPE = (
        (1, 'Яндекс'),
        (2, 'Google')
    )
    id = models.IntegerField(db_index=True, primary_key=True)
    name = models.CharField(max_length=250, verbose_name='Name', null=True, blank=True)

    user = models.ForeignKey(UserProfile, related_name='campaign_user', blank=True, null=True)
    topic = models.ForeignKey(Topics, related_name='campaign_topic')

    type = models.IntegerField('Type campaign', default=1, choices=CHOICE_TYPE)

    expense = models.IntegerField('Expense', default=0, blank=True)

    class Meta:
        verbose_name = 'campaign'
        verbose_name_plural = 'Campaigns'

"""
#
# Bids
#
"""
class Bids(BaseModel):
    CHOICE_TYPE = (
        (1, 'С сайта'),
        (2, 'Вручную')
    )
    CHOICE_TYPE_BID = (
        (1, 'Заявка'),
        (2, 'Звонок')
    )
    CHOICE_STATUS = (
        (1, 'Модерация'),
        (2, 'Одобрено'),
        (3, 'Брак')
    )
    #user = models.ForeignKey(UserProfile, related_name='bids_user', blank=True, null=True)
    name_customer = models.CharField(max_length=255, blank=True, null=True)
    email_customer = models.EmailField(blank=True, null=True)
    phone_customer = models.CharField(max_length=255, blank=True, null=True)
    city = models.CharField(max_length=255, blank=True, null=True)
    extra_info = models.TextField(blank=True, null=True)

    type = models.IntegerField('Type', default=1, choices=CHOICE_TYPE)
    type_bid = models.IntegerField('Type bids', default=1, choices=CHOICE_TYPE_BID)

    http_refer = models.CharField(max_length=555, blank=True, null=True)
    ip = models.GenericIPAddressField(blank=True, null=True)
    url_path = models.CharField(max_length=555, blank=True, null=True)
    utm_campaign = models.CharField(max_length=555, blank=True, null=True)
    utm_medium = models.CharField(max_length=555, blank=True, null=True)
    utm_term = models.CharField(max_length=555, blank=True, null=True)
    utm_page = models.CharField(max_length=1200, blank=True, null=True)

    status = models.IntegerField(choices=CHOICE_STATUS, null=True, blank=True)


    topic = models.ManyToManyField(Topics, related_name='bids_topic', blank=True)

    commentary = models.TextField(max_length=1000, blank=True, null=True)

    cost = models.IntegerField(blank=True, null=True)

"""
#
# User status
#
"""
CHOICE_STATUS_DEFAULT = (
        (1, 'В работе'),
        (2, 'Недозвон'),
        (3, 'Перезвонить'),
        (4, 'Продано'),
        (5, 'Отказано')
)

class StatusBids(models.Model):
    value = models.IntegerField(default=1, blank=True, null=True)
    choice = models.CharField(max_length=200, blank=True, null=True)

"""
#
# User settings
#
"""
class UserSettings(models.Model):
    user = models.ForeignKey(UserProfile, verbose_name='User', related_name='user_settings_user')
    topics = models.ManyToManyField(Topics)

    status = models.ManyToManyField(StatusBids)

    city = models.CharField(max_length=255, blank=True, null=True)# City
    address = models.CharField(max_length=1055, blank=True, null=True)# Address
    mode_job = models.CharField(max_length=500, blank=True, null=True)# Режим работы
    site_url = models.URLField(blank=True, null=True)# Сайт

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)


"""
#
# User bids
#
"""
class UserBids(BaseModel):
    user = models.ForeignKey(UserProfile, verbose_name='User', related_name='user_user_bids')
    bid = models.ForeignKey(Bids, verbose_name="User bid", related_name='bids_user_bids')

    topic = models.ManyToManyField(Topics, related_name='user_bids_topic', blank=True)

    status_callback = models.ForeignKey(StatusBids, null=True, blank=True)
    email_customer = models.EmailField(blank=True, null=True)
    name_customer = models.CharField(max_length=255, blank=True, null=True)
    phone_callback = models.CharField(max_length=255, blank=True, null=True)
    phone_callback_two = models.CharField(max_length=255, blank=True, null=True)

    commentary = models.TextField(max_length=1000, blank=True, null=True)

    cost = models.IntegerField(blank=True, null=True)
    bought = models.BooleanField(default=True, blank=True)

    # return date ago
    def date(self):
        return self.bid.created.day

"""
#
# Products and service(user)
#
"""
def screen_upload_path(instance, filename):
    """Generates upload path for FileField"""
    return u"%s/%s" % (instance.user.id, filename)

class ProductsAndService(BaseModel):
    user = models.ForeignKey(UserProfile, verbose_name='User', related_name='user_user_proandser')

    name = models.CharField(max_length=255)
    price_from = models.IntegerField()
    price_to = models.IntegerField(blank=True, null=True)

    url = models.URLField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)

    image = ImageField(upload_to=screen_upload_path, blank=True, null=True)


"""
#
# Promocode
#
"""
class Promocode(BaseModel):
    user = models.ForeignKey(UserProfile, related_name='user_user_promocode', blank=True, null=True)

    title = models.CharField(max_length=255, blank=True, null=True)
    code = models.CharField(max_length=12, blank=True, null=True)

    topic = models.ManyToManyField(Topics, related_name='user_topic_promocode', blank=True)

    start_at = models.DateTimeField(blank=True, null=True)
    end_at = models.DateTimeField(blank=True, null=True)

"""
#
# User session
#
"""
"""
class UserSession(models.Model):
    user = models.ForeignKey(UserProfile, blank=True, null=True)
    session_id = models.CharField(max_length=40)
    session = models.ForeignKey(Session, blank=True, null=True)
"""

"""
#
# Function calculation cpl
#
"""
"""
def calculation_cpl(sender, **kwargs):
    bid = kwargs["instance"]
    if kwargs["created"]:
        extra_price = bid.user.extra_price#берем наценку спл из личного кабинета
        expense = 0#собираем расходы по тематике
        count_bids = 0#собираем количество заявок по тематике

        for topic in bid.topic.all():
             expense += topic.expense()
             count_bids += Bids.objects.filter(topic=topic).count()

        cpl = ((extra_price*100) / (expense / count_bids)) + (expense / count_bids)

        if bid.user.is_autoclick:# автосписание
            user = UserProfile.objects.get(pk=bid.user.pk)
            if user.balance >= cpl and cpl != 0:
                user.balance -= cpl
                user.save()
                bid.bought = True
                bid.save()
        else:# без автосписания
            bid.cost = cpl
            bid.save()

    else:
        print('errors')

post_save.connect(calculation_cpl, sender=UserBids)
"""
"""
#
# Function добавление заявок пользователям
#
"""
def add_bids_user(sender, **kwargs):
    bid = kwargs["instance"]
    if kwargs["created"] and bid.is_active:
        """
        for topic in bid.topic.all():
            users = UserSettings.objects.filter(topic__in=topic)
            for user in users:
                bids = UserBids.objects.create(user=user, bid=bid)
                bids.topic.add(topic)
                bids.save()
        """
    else:
        print('errors')

post_save.connect(add_bids_user, sender=Bids)

"""
def user_logged_in_handler(sender, request, user, **kwargs):
    UserSession.objects.get_or_create(user=user, session_id = request.session.session_key)

user_logged_in.connect(user_logged_in_handler)
"""

"""
#
# Create user settings
#
"""
def add_settings_user(sender, **kwargs):
    user = kwargs["instance"]
    if kwargs["created"]:
        users = UserSettings.objects.create(user=user)

post_save.connect(add_settings_user, sender=UserProfile)



"""
#
# After success payment
#
"""
def after_succes_payment(sender, **kwargs):
    pay = kwargs["instance"]
    if pay.status == 'success':
        user = UserProfile.objects.get(id=pay.user.id)
        user.balance = user.balance + pay.order_amount
        user.save()

post_save.connect(after_succes_payment, sender=Payment)