from rest_framework import serializers
from core.models import ProductsAndService, UserBids, Bids, Topics, StatusBids


class StatusSerializer(serializers.ModelSerializer):
    pk = serializers.IntegerField(read_only=True)

    class Meta:
        model = StatusBids
        fields = ('pk', 'value', 'choice')


class TopicSerializer(serializers.ModelSerializer):
    pk = serializers.IntegerField(read_only=True)

    class Meta:
        model = Topics
        fields = ('pk', 'name')

class BidSerializer(serializers.ModelSerializer):
    pk = serializers.IntegerField(read_only=True)
    topic = TopicSerializer(many=True, read_only=True)

    date = serializers.SerializerMethodField('daty')

    def daty(self, obj):
        return obj.created.strftime("%d.%m.%Y")

    class Meta:
        model = Bids
        fields = ('pk', 'name_customer', 'phone_customer', 'email_customer', 'city', 'extra_info', 'type', 'type_bid', 'status',
                  'topic', 'commentary', 'cost', 'created', 'date')

class ProductsAndServiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductsAndService
        fields = ('name', 'price_from', 'price_to', 'url', 'description', 'image')
        #exclude = ('discount', 'calendar_image', 'color', 'logo', 'is_expired', 'gallery')

class UserBidsSerializer(serializers.ModelSerializer):
    pk = serializers.IntegerField(read_only=True)
    bid = BidSerializer(many=False, read_only=True)
    topic = TopicSerializer(many=True, read_only=True)
    status_callback = StatusSerializer(many=False, read_only=True)

    date = serializers.SerializerMethodField('daty')

    def daty(self, obj):
        return obj.created.strftime("%d.%m.%Y")

    class Meta:
        model = UserBids
        fields = ('pk', 'bid', 'topic', 'status_callback', 'name_customer', 'email_customer', 'phone_callback',
                  'commentary', 'phone_callback_two', 'cost', 'bought', 'created', 'date')