# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_auto_20150805_0033'),
    ]

    operations = [
        migrations.AddField(
            model_name='statusbids',
            name='value',
            field=models.IntegerField(default=1, null=True, blank=True),
        ),
    ]
