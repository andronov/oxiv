# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Bids',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('is_active', models.BooleanField(default=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('name_customer', models.CharField(max_length=255, null=True, blank=True)),
                ('phone_customer', models.CharField(max_length=255, null=True, blank=True)),
                ('city', models.CharField(max_length=255, null=True, blank=True)),
                ('type', models.IntegerField(default=1, verbose_name=b'Type', choices=[(1, b'\xd0\xa1 \xd1\x81\xd0\xb0\xd0\xb9\xd1\x82\xd0\xb0'), (2, b'\xd0\x92\xd1\x80\xd1\x83\xd1\x87\xd0\xbd\xd1\x83\xd1\x8e')])),
                ('type_bid', models.IntegerField(default=1, verbose_name=b'Type bids', choices=[(1, b'\xd0\xa1 \xd1\x81\xd0\xb0\xd0\xb9\xd1\x82\xd0\xb0'), (2, b'\xd0\x92\xd1\x80\xd1\x83\xd1\x87\xd0\xbd\xd1\x83\xd1\x8e')])),
                ('commentary', models.TextField(max_length=1000, null=True, blank=True)),
                ('cost', models.IntegerField(null=True, blank=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Campaigns',
            fields=[
                ('is_active', models.BooleanField(default=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('id', models.IntegerField(serialize=False, primary_key=True, db_index=True)),
                ('name', models.CharField(max_length=250, null=True, verbose_name=b'Name', blank=True)),
                ('type', models.IntegerField(default=1, verbose_name=b'Type campaign', choices=[(1, b'\xd0\xaf\xd0\xbd\xd0\xb4\xd0\xb5\xd0\xba\xd1\x81'), (2, b'Google')])),
            ],
            options={
                'verbose_name': 'campaign',
                'verbose_name_plural': 'Campaigns',
            },
        ),
        migrations.CreateModel(
            name='Topics',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('is_active', models.BooleanField(default=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('name', models.CharField(max_length=250, verbose_name=b'Name')),
            ],
            options={
                'verbose_name': 'topic',
                'verbose_name_plural': 'topics',
            },
        ),
        migrations.CreateModel(
            name='UserBids',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('is_active', models.BooleanField(default=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('status_callback', models.IntegerField(default=1, verbose_name=b'status callback', choices=[(1, b'\xd0\x92 \xd1\x80\xd0\xb0\xd0\xb1\xd0\xbe\xd1\x82\xd0\xb5'), (2, b'\xd0\x9d\xd0\xb5\xd0\xb4\xd0\xbe\xd0\xb7\xd0\xb2\xd0\xbe\xd0\xbd'), (3, b'\xd0\x9f\xd0\xb5\xd1\x80\xd0\xb5\xd0\xb7\xd0\xb2\xd0\xbe\xd0\xbd\xd0\xb8\xd1\x82\xd1\x8c'), (4, b'\xd0\x9f\xd1\x80\xd0\xbe\xd0\xb4\xd0\xb0\xd0\xbd\xd0\xbe'), (5, b'\xd0\x9e\xd1\x82\xd0\xba\xd0\xb0\xd0\xb7\xd0\xb0\xd0\xbd\xd0\xbe')])),
                ('phone_callback', models.CharField(max_length=255, null=True, blank=True)),
                ('bid', models.ForeignKey(related_name='bids_user_bids', verbose_name=b'User bid', to='core.Bids')),
                ('topic', models.ManyToManyField(related_name='user_bids_topic', to='core.Topics', blank=True)),
                ('user', models.ForeignKey(related_name='user_user_bids', verbose_name=b'User', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='UserSettings',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('topics', models.ManyToManyField(to='core.Topics')),
                ('user', models.ForeignKey(related_name='user_settings_user', verbose_name=b'User', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='campaigns',
            name='topic',
            field=models.ForeignKey(related_name='campaign_topic', to='core.Topics'),
        ),
        migrations.AddField(
            model_name='campaigns',
            name='user',
            field=models.ForeignKey(related_name='campaign_user', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='bids',
            name='topic',
            field=models.ForeignKey(related_name='bids_topic', to='core.Topics'),
        ),
    ]
