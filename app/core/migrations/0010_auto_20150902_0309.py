# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0009_auto_20150826_1723'),
    ]

    operations = [
        migrations.AddField(
            model_name='bids',
            name='email_customer',
            field=models.EmailField(max_length=254, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='userbids',
            name='email_customer',
            field=models.EmailField(max_length=254, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='userbids',
            name='name_customer',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
    ]
