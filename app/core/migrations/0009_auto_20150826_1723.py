# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0008_promocode'),
    ]

    operations = [
        migrations.AddField(
            model_name='bids',
            name='extra_info',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='bids',
            name='http_refer',
            field=models.CharField(max_length=555, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='bids',
            name='ip',
            field=models.GenericIPAddressField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='bids',
            name='status',
            field=models.IntegerField(blank=True, null=True, choices=[(1, b'\xd0\x9c\xd0\xbe\xd0\xb4\xd0\xb5\xd1\x80\xd0\xb0\xd1\x86\xd0\xb8\xd1\x8f'), (2, b'\xd0\x9e\xd0\xb4\xd0\xbe\xd0\xb1\xd1\x80\xd0\xb5\xd0\xbd\xd0\xbe'), (3, b'\xd0\x91\xd1\x80\xd0\xb0\xd0\xba')]),
        ),
        migrations.AddField(
            model_name='bids',
            name='url_path',
            field=models.CharField(max_length=555, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='bids',
            name='utm_campaign',
            field=models.CharField(max_length=555, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='bids',
            name='utm_medium',
            field=models.CharField(max_length=555, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='bids',
            name='utm_page',
            field=models.CharField(max_length=1200, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='bids',
            name='utm_term',
            field=models.CharField(max_length=555, null=True, blank=True),
        ),
    ]
