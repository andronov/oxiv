# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('core', '0007_productsandservice'),
    ]

    operations = [
        migrations.CreateModel(
            name='Promocode',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('is_active', models.BooleanField(default=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('title', models.CharField(max_length=255, null=True, blank=True)),
                ('code', models.CharField(max_length=12, null=True, blank=True)),
                ('start_at', models.DateTimeField(null=True, blank=True)),
                ('end_at', models.DateTimeField(null=True, blank=True)),
                ('topic', models.ManyToManyField(related_name='user_topic_promocode', to='core.Topics', blank=True)),
                ('user', models.ForeignKey(related_name='user_user_promocode', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
