# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='campaigns',
            name='expense',
            field=models.IntegerField(default=0, verbose_name=b'Expense', blank=True),
        ),
        migrations.AddField(
            model_name='userbids',
            name='bought',
            field=models.BooleanField(default=True),
        ),
        migrations.AddField(
            model_name='userbids',
            name='commentary',
            field=models.TextField(max_length=1000, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='userbids',
            name='cost',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.RemoveField(
            model_name='bids',
            name='topic',
        ),
        migrations.AddField(
            model_name='bids',
            name='topic',
            field=models.ManyToManyField(related_name='bids_topic', to='core.Topics', blank=True),
        ),
        migrations.AlterField(
            model_name='bids',
            name='type_bid',
            field=models.IntegerField(default=1, verbose_name=b'Type bids', choices=[(1, b'\xd0\x97\xd0\xb0\xd1\x8f\xd0\xb2\xd0\xba\xd0\xb0'), (2, b'\xd0\x97\xd0\xb2\xd0\xbe\xd0\xbd\xd0\xbe\xd0\xba')]),
        ),
    ]
