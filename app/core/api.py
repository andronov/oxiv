# -*- coding: utf-8 -*-
import json
import urllib2
from django.conf import settings
from core.models import Bids

"""
#
# Yandex API
#
"""
# In developing
class YandexApi():
    url = 'https://api.direct.yandex.ru/v4/json/'
    token = settings.TOKEN_YANDEX
    login = settings.LOGIN_YANDEX

    def __init__(self, *args, **kwargs):
        self.method = kwargs.pop('method', 'GetSummaryStat')

    def result(self):
        data = {
           'method': self.method,
           'token': self.token,
           'locale': 'ru',
           'param': {"CampaignIDS": [13675267], "StartDate": '2015-07-28', "EndDate": '2015-07-30'}
        }

        jdata = json.dumps(data, ensure_ascii=False).encode('utf8')

        response = urllib2.urlopen(self.url, jdata)
        return response.read().decode('utf8')
#Example
#res = YandexApi(method='GetClientInfo') or YandexApi()
#res.result()

"""
#
# Google API
#
"""
# In developing
class GoogleApi():
    url = 'https://adwords.google.com/api/adwords/reportdownload/v201402'
    access_token = settings.GOOGLE_OAUTH2_ACCESS_TOKEN
    developer_token = settings.GOOGLE_DEVELOPER_TOKEN
    clien_customer_id = settings.GOOGLE_CLIENT_CUSTOMER_ID

    #def __init__(self, *args, **kwargs):
        #self.method = kwargs.pop('method', 'GetClientInfo')

    def process(self):
        data = {
           'OAUTH2_ACCESS_TOKEN': self.access_token,
           'DEVELOPER_TOKEN': self.developer_token,
           'CLIENT_CUSTOMER_ID': self.clien_customer_id
        }

        jdata = json.dumps(data, ensure_ascii=False).encode('utf8')

        response = urllib2.urlopen(self.url, jdata)

        return self.xmltojson(response.read().decode('utf8'))

    def xmltojson(self, data):
        # return in json data
        pass


"""
#
# Get bids from the site and etc
#
"""
# In developing
def get_bids(request):
    # тут условия проверки если все проходит,создаем заявку
    bid = Bids.objects.create(is_active=False, name_customer='test', phone_customer='test', city='test', type=2,
                              type_bid=2)