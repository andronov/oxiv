from django.conf.urls import patterns, url
from core.views import *

urlpatterns = patterns('',
    url(r'^$', HomePageView.as_view(), name='test'),

    url(r'^test/$', test, name='test'),

    url(r'^leads/$', UserBidsListView.as_view(), name='user_leads'),
    url(r'^stats/$', UserStatisticsView.as_view(), name='user_stats'),
    url(r'^settings/$', UserSettingsView.as_view(), name='user_settings'),

    url(r'^company/$', UserProfileView.as_view(), name='user_company'),
    url(r'^company/form/sidebar/$', company_form_sidebar, name='user_company_form_sidebar'),
    url(r'^company/form/popup/add/$', company_form_popup_add, name='user_company_form_popup_add'),

    url(r'^billing/payment-form/$', order_page, name='user_billing_order'),
    url(r'^billing/$', UserBalanceView.as_view(), name='user_billing'),


    #url(r'^bids/commentary/(?P<id>.*)/save/$', user_bid_commentary_save(), name='user_commentary_bid_save'),


)