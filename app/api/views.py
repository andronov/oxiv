import json
from django.db.models.signals import post_save
from django.template.loader import render_to_string
from django.views.generic import TemplateView
from rest_framework import viewsets
from ws4redis.publisher import RedisPublisher
from ws4redis.redis_store import RedisMessage
from core.models import ProductsAndService, Bids, UserBids, UserSettings
from core.serializers import ProductsAndServiceSerializer
from ws4redis.redis_store import SELF

class BroadcastChatView(TemplateView):
    template_name = 'base.html'

    def get(self, request, *args, **kwargs):
        #welcome = RedisMessage('Hello everybody')  # create a welcome message to be sent to everybody
        #RedisPublisher(facility='foobar', broadcast=True).publish_message(welcome)
        return super(BroadcastChatView, self).get(request, *args, **kwargs)



def add_bids_user(sender, **kwargs):
    bid = kwargs["instance"]
    if kwargs["created"] and bid.is_active:
        redis_publisher = RedisPublisher(facility='usersleads', users=[str(bid.user.username)])

        lead = UserBids.objects.get(id=bid.id)
        settings = UserSettings.objects.get(user=lead.user)
        data = {}
        context = {'id': lead.id, 'bid': lead, 'settings': settings}
        data['html'] = render_to_string("core/leads/bids_leads_only.html", context)
        data_for_websocket = json.dumps(data)

        redis_publisher.publish_message(RedisMessage(data_for_websocket))
    else:
        print('errors')

post_save.connect(add_bids_user, sender=UserBids)