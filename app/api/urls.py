from django.conf.urls import patterns, url
from api.views import *

urlpatterns = patterns('',
    url(r'^$', BroadcastChatView.as_view(), name='api_home'),
)