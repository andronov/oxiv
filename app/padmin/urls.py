from django.conf.urls import patterns, url
from padmin.views import *

urlpatterns = patterns('',
    url(r'^$', AdminUserListView.as_view(), name='admin_users'),

    url(r'^stats/$', AdminStatisticsView.as_view(), name='admin_stats'),
    url(r'^leads/$', AdminLeadsView.as_view(), name='admin_leads'),
    url(r'^partners/$', AdminPartnersView.as_view(), name='admin_partners'),
    url(r'^settings/$', AdminSettingsView.as_view(), name='admin_settings'),


    #url(r'^bids/$', UserBidsListView.as_view(), name='user_bids'),



)