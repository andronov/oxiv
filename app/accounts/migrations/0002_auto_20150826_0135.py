# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='usernotification',
            old_name='noti_sound',
            new_name='sound',
        ),
        migrations.RemoveField(
            model_name='usernotification',
            name='noti_email',
        ),
        migrations.RemoveField(
            model_name='usernotification',
            name='noti_site',
        ),
        migrations.AlterField(
            model_name='usernotification',
            name='email',
            field=models.BooleanField(default=True, verbose_name=b'Email noti'),
        ),
    ]
