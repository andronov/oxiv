# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0002_auto_20150826_0135'),
    ]

    operations = [
        migrations.RenameField(
            model_name='usernotification',
            old_name='sound',
            new_name='noti_sound',
        ),
        migrations.AddField(
            model_name='usernotification',
            name='noti_email',
            field=models.BooleanField(default=True, verbose_name=b'Email noti'),
        ),
        migrations.AddField(
            model_name='usernotification',
            name='noti_site',
            field=models.BooleanField(default=True, verbose_name=b'noti'),
        ),
        migrations.AlterField(
            model_name='usernotification',
            name='email',
            field=models.EmailField(max_length=254, null=True, blank=True),
        ),
    ]
