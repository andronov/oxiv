# -*- coding: utf-8 -*-
import json
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.views.decorators.csrf import csrf_protect
from registration.models import RegistrationProfile
from accounts.models import UserProfile


import hashlib
import time
def _createPass():
    """
    Function for creating 6 symbols random hash for referral code
    """
    hash = hashlib.md5()
    hash.update(str(time.time()))
    return hash.hexdigest()[:6]

def _createHash():
    """
    Function for creating 6 symbols random hash for referral code
    """
    hash = hashlib.md5()
    hash.update(str(time.time()))
    return hash.hexdigest()[:7]

#registration
#@csrf_protect
def new_registration_user(request):
    data = {}
    email = request.GET['email']

    if email:
        try:
              UserProfile.objects.get(email=email)
              data['error'] = 'Пользователь с таким электронным адресом уже существует!'
              return HttpResponse(json.dumps({'data':data}), content_type="application/json")
        except UserProfile.DoesNotExist:
            new_user = RegistrationProfile.objects.create_inactive_user(username=_createHash(),
                        first_name=u'test',
                        last_name=u'test',
                        password=_createPass(),
                        email=email,
                        site='')

            print(new_user)
            """
            text_content = 'Данные с доступом к счету активного билета'
            html_content = render_to_string('mailchimp/notification.html', {'users': users, 'ticket': ticket})
            msg = EmailMultiAlternatives('Данные с доступом к счету активного билета', text_content, settings.EMAIL_HOST_USER,  [users.email])
            msg.attach_alternative(html_content, "text/html")
            msg.send()
            """
            data['ok'] = 'Спасибо Вам за регистрацию!'

    else:
        data['error'] = 'Вы ввели неправильный адрес электронной почты!'
    return HttpResponse(json.dumps({'data':data}), content_type="application/json")
